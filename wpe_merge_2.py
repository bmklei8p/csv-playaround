import csv
import json

def wpe_merge(csv_file, output_csv_file_name):
    h = {}
    data = []
    with open(csv_file, 'r') as file:
        csvreader = csv.reader(file)
        header = next(csvreader)
        header.append("Status")
        header.append("Status Set On")
        # response = requests.get(url)  ##API pull request comes back with a JSON string
        # items = json.loads(response.content) ##response.content comes back with the content of the API request. JSON.loads converts JSON string into a dictionary
        items = {'count': 439, 'next': 'http://interview.wpengine.io/v1/accounts/?limit=20&offset=20', 'previous': None, 'results': [{'account_id': 314159, 'status': 'good', 'created_on': '2012-01-12'}, {'account_id': 271, 'status': 'good', 'created_on': '2011-03-22'}, {'account_id': 8675309, 'status': 'closed', 'created_on': '2014-12-21'}, {'account_id': 99999, 'status': 'fraud', 'created_on': '2014-09-17'}, {'account_id': 12345, 'status': 'good', 'created_on': '2011-01-12'}, {'account_id': 8172, 'status': 'closed', 'created_on': '2015-09-01'}, {'account_id': 1924, 'status': 'fraud', 'created_on': '2012-03-01'}, {'account_id': 222222, 'status': 'poor', 'created_on': '2014-01-02'}, {'account_id': 666, 'status': 'high risk', 'created_on': '2015-04-01'}, {'account_id': 48213, 'status': 'high risk', 'created_on': '2015-08-15'}, {'account_id': 918299, 'status': 'good', 'created_on': '2014-06-01'}, {'account_id': 88888, 'status': 'collections', 'created_on': '2015-08-08'}, {'account_id': 99, 'status': 'good', 'created_on': '2012-01-12'}, {'account_id': 798979, 'status': 'good', 'created_on': '2012-01-12'}, {'account_id': 0, 'status': 'good', 'created_on': '2012-01-12'}, {'account_id': 27, 'status': 'awesome', 'created_on': '2012-01-12'}, {'account_id': 7, 'status': 'awesome', 'created_on': '2012-01-12'}, {'account_id': 272727, 'status': 'awesome', 'created_on': '2020-10-19'}, {'account_id': 200, 'status': 'awesome', 'created_on': '2020-10-10'}, {'account_id': 201, 'status': 'awesome', 'created_on': '2020-10-10'}]}

        ## loop over the results dictionaries and create a has map of the account ID's as keys and the full account object as the value
        for i in items["results"]:
            h[i.get("account_id")] = i

        ## because the data for the write function has to be a list of lists we are adding status and status_set_on to the row and then appending the whole row to the data list
        for row in csvreader:
            obj_of_account_row = h.get(int(row[0]))
            status = obj_of_account_row.get("status")
            status_set_on = obj_of_account_row.get("created_on")
            row.append(status)
            row.append(status_set_on)
            data.append(row)

    ## write to a new file - the data being inputed for this method to work is a list of lists where each inner list is a row of the file
    with open(output_csv_file_name, 'w', newline='') as file:
        writer = csv.writer(file)
        # write the header
        writer.writerow(header)
        # write all of the rows with the data
        writer.writerows(data)
    ## this return statment isn't necessary I think
    return print('Complete')

x = wpe_merge("CSV_playaround.csv", "CSV_output.csv")
