from django.db import models

class Account(models.Model):
    account_id = models.IntegerField()
    status = models.TextField()
    created_on = models.DateField()
