
import csv
import json

def wpe_merge(csv_file, output_csv_file_name):
    h = {}
    ## open csv file as read and set it to file
    with open(csv_file, 'r') as file:
        #read the contents of file and set the content to csvreader
        csvreader = csv.reader(file)
        ## build a header list - These are hard coded because Status Set on is the desired title but it is not a title in the API
        header = next(csvreader)
        header.append("Status")
        header.append("Status Set On")
        ## do the API request and set the resulting JSON string to a response variable
        # response = requests.get(url)  ##API pull request comes back with a JSON string
        ## convert the JSON string into a python dictionary
        # items = json.loads(response.content) ##response.content comes back with the content of the API request. JSON.loads converts JSON string into a dictionary
        items = {'count': 439, 'next': 'http://interview.wpengine.io/v1/accounts/?limit=20&offset=20', 'previous': None, 'results': [{'account_id': 314159, 'status': 'good', 'created_on': '2012-01-12'}, {'account_id': 271, 'status': 'good', 'created_on': '2011-03-22'}, {'account_id': 8675309, 'status': 'closed', 'created_on': '2014-12-21'}, {'account_id': 99999, 'status': 'fraud', 'created_on': '2014-09-17'}, {'account_id': 12345, 'status': 'good', 'created_on': '2011-01-12'}, {'account_id': 8172, 'status': 'closed', 'created_on': '2015-09-01'}, {'account_id': 1924, 'status': 'fraud', 'created_on': '2012-03-01'}, {'account_id': 222222, 'status': 'poor', 'created_on': '2014-01-02'}, {'account_id': 666, 'status': 'high risk', 'created_on': '2015-04-01'}, {'account_id': 48213, 'status': 'high risk', 'created_on': '2015-08-15'}, {'account_id': 918299, 'status': 'good', 'created_on': '2014-06-01'}, {'account_id': 88888, 'status': 'collections', 'created_on': '2015-08-08'}, {'account_id': 99, 'status': 'good', 'created_on': '2012-01-12'}, {'account_id': 798979, 'status': 'good', 'created_on': '2012-01-12'}, {'account_id': 0, 'status': 'good', 'created_on': '2012-01-12'}, {'account_id': 27, 'status': 'awesome', 'created_on': '2012-01-12'}, {'account_id': 7, 'status': 'awesome', 'created_on': '2012-01-12'}, {'account_id': 272727, 'status': 'awesome', 'created_on': '2020-10-19'}, {'account_id': 200, 'status': 'awesome', 'created_on': '2020-10-10'}, {'account_id': 201, 'status': 'awesome', 'created_on': '2020-10-10'}]}

        ## loop over the results dictionaries and create a has map of the account ID's as keys and the full account object as the value
        for i in items["results"]:
            h[i.get("account_id")] = i

        ## This was to create a string with page line breaks as final output as that what we thought was desired to write the csv file
        # join the header list into a string seperated by ','
        output = ",".join(header)
        # end this string with a newline to denote the next row
        output += '\n'
        #loop over the rows of the csv file
        for row in csvreader:
            ## join these rows into a string seperated by ','
            str_row = ",".join(row)
            ## at these strings to the result string
            output += str_row + ','
            ## search the hash map for the key (which is an int) and if it is in the hash map do the following:
            if h.get(int(row[0])) is not None:
                # get the object that is the value of the key
                obj_of_account_row = h.get(int(row[0]))
                #add the status to the result string
                output += obj_of_account_row.get("status") + ','
                #add the created on date to the result string and then added a newline to start the next row
                output += obj_of_account_row.get("created_on") + '\n'
    return output


x = wpe_merge("CSV_playaround.csv", "CSV_output")
print(x)
